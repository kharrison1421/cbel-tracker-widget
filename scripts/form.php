<?php
// define variables and set to empty
$nameErr = $companyErr = $phonenumErr = $emailErr = $ideaErr = "";
$name = $company = $phonenum = $email = $idea = "";

// checks to see if fields are filled in correctly
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    // Required. Checks name against regex.
    if (empty($_POST["name"])) {
        $nameErr = "Name is required.";
    } else {
        $name = test_input($_POST["name"]);
        if (!preg_match("/^[\p{L} \.'\-]+$/", $name)) {
            $nameErr = "Please enter a valid username.";
        }
    }
    
    // Not Required. Checks name against regex.
    $company = test_input($_POST["company"]);
    
    if ($company != "") {
        if (!preg_match("/^[A-Z]([a-zA-Z0-9]|[- @\.#&!])*$/", $company)) {
            $companyErr = "Please enter a valid company.";
        } 
    }
    
    // Not Required. Phone Number should only contain numbers and dashes.
    $phonenum = test_input($_POST["phonenum"]);
    
    if ($phonenum != ""){
        if (!preg_match("/^[\d\-]+$/", $phonenum)) {
            $phonenumErr = "Please enter a valid phone number.";
        }
    }

    // Required. Check email against regex.
    if (empty($_POST["email"])) {
        $emailErr = "Email is required.";
    } else {
        $email = test_input($_POST["email"]);
        if (!preg_match("/^[\S]+[@]{1}([\S]+[.]+[\S]+)+$/", $email)){
            $emailErr = "Please enter a valid email.";
        }
    }
    
    // Validating the email
    if ($emailErr == "") {
        $mailcheck = spamcheck($email);
        if ($mailcheck == FALSE) {
            $emailErr = "Invalid Input";
        }
    }
    
    // Required.
    if (empty($_POST["idea"])) {
        $ideaErr = "Idea is required.";
    } else {
        $idea = test_input($_POST["idea"]);
    }
}

// Cleans data to ensure that fields does not inject code.

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}


// Sanitizes and checks to see if email is valid.
function spamcheck($field)
{
    //Sanitize e-mail address
    $field = filter_var($field, FILTER_SANITIZE_EMAIL);
    //Validate e-mail address
    if (filter_var($field, FILTER_VALIDATE_EMAIL)) {
        return TRUE;
    } else {
        return FALSE;
    }
}
?>

<html>
    <head>
        <title>Public Widget</title>
        <meta charset="windows-1252">
        <meta name="viewport" content="width=device-width">
        <style>
            //Error message color.
            .error {color: #FF0000;}
            </style>
    </head>
    <body>
        
        <?php
        // display form if user has not clicked submit
        if (!isset($_POST["submit"]) || !(($ideaErr == "") & ($nameErr=="") & ($emailErr=="") & ($phonenumErr=="") & ($companyErr=="")))
            {
        ?>
    
        <!-- The form -->
        <form method="post" action="
            <?php  echo htmlspecialchars($_SERVER["PHP_SELF"]);?>
            <?php //verify.php; ?>">
            <span class="error">* required field</span><br><br>
            Name: <input type="text" name="name" value="<?php echo $name;?>">
            <span class="error">* <?php echo $nameErr;?></span><br>
            Group: <input type="text" name="company" value="<?php echo $company;?>">
            <span class="error"> <?php echo $companyErr;?></span><br>
            Phone Number: <input type="text" name="phonenum" value="<?php echo $phonenum;?>">
            <span class="error"> <?php echo $phonenumErr;?></span><br>
            E-mail: <input type="text" name="email" value="<?php echo $email;?>">
            <span class="error">* <?php echo $emailErr;?></span><br><br>
            Project Idea: 
            <span class="error">* <?php echo $ideaErr;?></span><br> 
            <textarea name="idea" rows="20" cols="40"><?php echo $idea;?></textarea><br>
            
            <?php
            // Captcha Implementation
            require_once('recaptchalib.php');
            // change this key if you changed accounts for captcha
            $publickey = "6LekHPASAAAAAJn1fEZ75Lcq9TO9mot1GTPLkF1K"; // you got this from the signup page
            echo recaptcha_get_html($publickey);
            ?>
            
            <input type="submit" name="submit" value="Send Email">
        </form>
        
        <?php
            }
            else {
            // the user has submitted to form, formats message
                if (($ideaErr == "") && ($nameErr=="") && ($emailErr=="") && ($phonenumErr=="") && ($companyErr=="")) {
                    $subject = "CBEL Tracker - New Contact";
                    $message = "From: " . $name . "\n\n".
                                "Group: " . $company . "\n\n" .
                                "E-mail: " . $email . "\n\n" .
                                "Phone Number: " . $phonenum . "\n\n" .
                                "Project Idea: \n" .
                                $idea;
                    //message lines should not exceed 70
                    $message = wordwrap($message, 70);
                    
                    //Validate Captcha
                    
                    require_once('recaptchalib.php');
                    // change this key if you've changed accounts
                    $privatekey = "6LekHPASAAAAADhJO7rbQTk-pT8JzhdJ4kTg9XzC";
                    $resp = recaptcha_check_answer ($privatekey,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);

                    if (!$resp->is_valid) {
                        // What happens when the CAPTCHA was entered incorrectly
                        die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
                            "(reCAPTCHA said: " . $resp->error . ")");
                        
                    } else {
                    
                    // validated, send mail
                    require("phpmailer/class.phpmailer.php");
                    $mail = new PHPMailer();

                    // ---------- adjust these lines ---------------------------------------
                    $mail->Username = "cbeltracker@gmail.com"; // your GMail user name
                    $mail->Password = "ccelrocks";
                    // Don't hack me bro.
                    $mail->AddAddress("cbeltracker@gmail.com"); // recipients email
                    $mail->FromName = $name; // readable name

                    $mail->Subject = "A New Company Wants to Start a Relationship with You";
                    $mail->Body    = $message; 
                    //-----------------------------------------------------------------------

                    $mail->Host = "ssl://smtp.gmail.com"; // GMail
                    $mail->Port = 465;
                    $mail->IsSMTP(); // use SMTP
                    $mail->SMTPAuth = true; // turn on SMTP authentication
                    $mail->From = $mail->Username;
                    if(!$mail->Send()){
                        echo "Error: " . $mail->ErrorInfo;
                    } else {
                        echo "Your message has been sent, we will contact you shortly.";
                        }
                    }
                    
                }
                    
                }
        ?>
        
    </body>
    
</html>
